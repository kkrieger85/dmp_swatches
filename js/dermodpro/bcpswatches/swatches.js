/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_BCP_Swatches
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */
if (typeof BCP != 'undefined' && typeof BCP.Config != 'undefined') {
	BCP.Swatches = Class.create(BCP.Config, {
		selectSimpleProduct: function(productId) {
			var currentProductId = this.getBcpCurrentProduct();
			if (productId != currentProductId) {
				this.setSimpleProduct(productId);
				this.reloadAllOptionLabels();
				this.setBcpCurrentProduct(currentProductId);
				this.updateProductView(productId);
			}
		}
	});

	/*
	 * Overwrite the original Product.Config class
	 */
	Product.Config = BCP.Swatches;
}