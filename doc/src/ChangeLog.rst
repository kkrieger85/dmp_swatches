.. footer::
   .. class:: footertable
   
   +-------------------------+-------------------------+
   | Stand: 14.05.2012       | .. class:: rightalign   |
   |                         |                         |
   |                         | ###Page###/###Total###  |
   +-------------------------+-------------------------+

.. sectnum::

====================================
BCP Swatches - Farb- oder Bildwähler
====================================

ChangeLog
=========

.. list-table::
   :header-rows: 1
   :widths: 1 1 6

   * - **Revision**
     - **Datum**
     - **Beschreibung**
   
   * - 12.10.08
     - 08.10.2012
     - Changes:

       * added LINCENSE.txt
       * changed lincense-header
    
   * - 0.1.8
     - 02.07.2012
     - Bugfixes:

       * Fixed: Remote urls were not taken as placeholder images
       * Fixed: "swatches.jpg" placeholder-image was missing
       * Fixed: Show price-label although "display price" is disabled
