<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_BCP_Swatches
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_BCP_Swatches_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
	 * Return the config value from the module config namespace for the specified key.
	 *
	 * @param string $key
	 * @param mixed $store
	 * @return string
	 */
	public function getConfig($key, $store = null)
	{
		return Mage::getStoreConfig('dermodpro_bcp/bcp_swatches/' . $key, $store = null);
	}

	/**
	 * Sets the bcp_swatch_image attribute on the product, depending on the source configuration.
	 * It is either:
	 * - an URL starting with 'http://'
	 * - an absolute path of the image file
	 * - an empty string ''
	 *
	 * @param Mage_Core_Model_Abstract $product
	 * @return DerModPro_BCP_Swatches_Helper_Data
	 */
	public function setSwatchImageOnProduct(Mage_Core_Model_Abstract $product)
	{
		$image = '';

		if (null === $product->getBcpSwatchImage())
		{
			$type = $this->getSwatchImageSourceType($product);
			switch ($type)
			{
				case 'url':
					$image = $product->getBcpSwatchImageUrl();
					if (substr($image, 0, 7) != 'http://' && substr($image, 0, 8) != 'https://')
					{
						$image = $this->getSwatchesImageDir($image);
					}
					break;
				case 'thumbnail':
					$image = $product->getThumbnail();
					break;
				case 'small_image':
					$image = $product->getSmallImage();
					break;
				case 'image':
					$image = $product->getImage();
					break;
				case 'gallery_last':
					$image = $this->_getGalleryImageFromProduct($product, 'last');
					break;
				case 'gallery_first':
					$image = $this->_getGalleryImageFromProduct($product, 'first');
					break;
			}
			//Mage::log(array($product->getSku() . ' ' . $type => $image));
			$product->setBcpSwatchImage($image);
		}

		return $this;
	}

	/**
	 * Return the directory within the media dir where manulally specified swatch images are expected.
	 * If an image file is expected it is appended to the directory path.
	 *
	 * @param string $image
	 * @return string
	 */
	public function getSwatchesImageDir($image = null)
	{
		$path = implode(DS, explode('/', $this->getConfig('swatch_image_dir')));
		if (substr($path, 0) !== DS)
		{
			$path = DS . $path;
		}
		if (isset($image))
		{
			if (substr($path, -1) != DS)
			{
				$path .= DS;
			}
			$path .= $image;
		}
		return $path;
	}

	/**
	 * Get the swatches image source type to use for this product, depending on the products and the system configuration.
	 *
	 * @param Varien_Object $product
	 * @return string
	 */
	public function getSwatchImageSourceType(Varien_Object $product)
	{
		if (is_null($product->getBcpSwatchImageSource()) && $product->getId())
		{
			$this->_loadSwatchImageSourceOnProduct($product);
		}

		$sourceType = $product->getBcpSwatchImageSource();
		if ($sourceType == '')
		{
			$sourceType = $this->getConfig('swatch_image_source');
		}

		return $sourceType;
	}

	/**
	 * Load the bcp_swatch_image_source attribute on a product model without loading the whole model.
	 * This should never be called, since we only use the BCP_Swatches on the product view page, but who
	 * knows where this will end.
	 *
	 * @param Mage_Catalog_Model_Product $product
	 * @return DerModPro_BCP_Swatches_Helper_Data
	 */
	protected function _loadSwatchImageSourceOnProduct(Mage_Catalog_Model_Product $product)
	{
		if (! $product->getId())
		{
			Mage::throwException($this->__('Unable to load bcp_swatch_image_source attribute on unloaded product.'));
		}
		$attribute = $product->getResource()->getAttribute('bcp_swatch_image_source');
		$select = $product->getResource()->getReadConnection()->select()
			->from($attribute->getBackendTable(), array('value'))
			->where('entity_type_id=?', $product->getResource()->getTypeId())
			->where('attribute_id=?', $attribute->getId())
			->where('entity_id=?', $product->getId())
			->where('store_id IN (?)', array(0, Mage::app()->getStore()->getId()))
			->order('store_id DESC')
			->limit(1)
		;
		$sourceType = $product->getResource()->getReadConnection()->fetchOne($select);
		$product->setBcpSwatchImageSource($sourceType);

		return $this;
	}

	/**
	 * Get the gallery image at the specified position from the product.
	 * Since we only use the BCP_Swatches on the product detail page this should not be required,
	 * but I load the gallery anyway if it isn't.
	 *
	 * @param Mage_Catalog_Model_Product  $product
	 * @param string|int $position 'first', 'last r numeric gallery image index
	 * @return string The full path of the image file, or '' if the file doesn't exist
	 */
	protected function _getGalleryImageFromProduct(Mage_Catalog_Model_Product $product, $position)
	{
		$image = '';
		if (! $product->getData('media_gallery'))
		{
			$attributes = $product->getTypeInstance(true)->getSetAttributes($product);
			if (isset($attributes['media_gallery']))
			{
				$attributes['media_gallery']->getBackend()->afterLoad($product);
			}
		}
		if ($mediaGallery = $product->getData('media_gallery'))
		{
			if (isset($mediaGallery['images']))
			{
				$info = false;
				if ($position == 'first')
				{
					$info = reset($mediaGallery['images']);
				}
				elseif ($position == 'last')
				{
					$info = end($mediaGallery['images']);
				}
				elseif (is_numeric($position) && isset($mediaGallery['images'][$position]))
				{
					$info = $mediaGallery['images'][$position];
				}
				if ($info)
				{
					$image = $info['file'];
				}
			}
		}
		return $image;
	}
}