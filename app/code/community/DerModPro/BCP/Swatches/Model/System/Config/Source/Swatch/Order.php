<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_BCP_Swatches
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_BCP_Swatches_Model_System_Config_Source_Swatch_Order
{
	/**
	 * Cache the options in memory
	 *
	 * @var array
	 */
    protected $_options;

	/**
	 * Return the available swatch image source options
	 *
	 * @return array
	 */
    public function toOptionArray()
    {
        if (is_null($this->_options)) {
            $this->_options = array(
				array(
					'label' => Mage::helper('bcp_swatches')->__('No Order'),
					'value' => '',
				),
				array(
					'label' => Mage::helper('bcp_swatches')->__('Simple Product ID'),
					'value' => 'product_id',
				),
				array(
					'label' => Mage::helper('bcp_swatches')->__('Simple Product Name'),
					'value' => 'name',
				),
				array(
					'label' => Mage::helper('bcp_swatches')->__('Simple Product SKU'),
					'value' => 'sku',
				),
				array(
					'label' => Mage::helper('bcp_swatches')->__('Swatch Image Name'),
					'value' => 'image_url',
				),
			);
        }

        return $this->_options;
    }
}