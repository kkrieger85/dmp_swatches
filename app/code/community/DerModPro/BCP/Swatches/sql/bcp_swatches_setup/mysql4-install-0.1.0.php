<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_BCP_Swatches
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

$this->startSetup();

$this->addAttribute('catalog_product', 'bcp_swatch_image_source', array(
	'label'			=> 'Swatches Image Source',
	'type'			=> 'varchar',
	'input'			=> 'select',
	'source'		=> 'bcp_swatches/entity_attribute_source_swatch_image',
	'global'		=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
	'required'		=> false,
	'default'		=> '',
	'user_defined'	=> 0,
	'apply_to'		=> Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
	'used_in_product_listing' => 0,
	'is_configurable' => 0,
	'filterable_in_search' => 0,
	'used_for_price_rules' => 0,
	'user_defined'  => 0,
));

$this->addAttribute('catalog_product', 'bcp_swatch_image_url', array(
	'label'			=> 'Swatches Image Path',
	'type'			=> 'varchar',
	'input'			=> 'text',
	'global'		=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
	'required'		=> false,
	'default'		=> '',
	'user_defined'	=> 0,
	'apply_to'		=> Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
	'used_in_product_listing' => 0,
	'is_configurable' => 0,
	'filterable_in_search' => 0,
	'used_for_price_rules' => 0,
	'user_defined'  => 0,
	'note'			=> 'Used for the "manual setting"'
));

$this->endSetup();